#!/bin/bash

set -eu

if [[ ! -f /app/data/configuration.js ]]; then
    echo "==> First run. Creating directories"
    mkdir -p /app/data/server-data
    cp /app/code/server/configuration.js_orig /app/data/configuration.js
fi

# had to set the app root manually because it would use the config path instead of /app/code
sed -i "s,const app_root =.*,const app_root = '/app/code';," /app/data/configuration.js

chown -R cloudron:cloudron /app/data/


echo "==> Starting WBO"
exec gosu cloudron:cloudron npm start

