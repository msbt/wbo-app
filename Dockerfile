FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=1.6.5

RUN curl -L https://github.com/lovasoa/whitebophir/archive/v${VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN mv /app/code/server/configuration.js /app/code/server/configuration.js_orig && \
    ln -s /app/data/configuration.js /app/code/server/configuration.js && \
    rm -rf /app/code/server-data/ && \
    ln -s /app/data/server-data /app/code/server-data

RUN chown -R cloudron:cloudron /app/code

RUN npm install --production

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
