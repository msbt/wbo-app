This app packages WBO

WBO is a free and open-source online collaborative whiteboard that allows many users to draw simultaneously on a large virtual board. The board is updated in real time for all connected users, and its state is always persisted. It can be used for many different purposes, including art, entertainment, design and teaching.

To collaborate on a drawing in real time with someone, just send them its URL.
